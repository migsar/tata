const { createHmac } = require('crypto');

function createSignature(method, endpoint, options) {
  const { params, secrets } = options;
  const parameterString = Object.entries(params)
    .map( entry => [
        encodeURIComponent(entry[0]),
        encodeURIComponent(entry[1]),
    ])
    .sort((a, b) => a[0].localeCompare(b[0]))
    .map(entry => `${entry[0]}=${entry[1]}`)
    .join('&');

  const signatureBaseString = [
    method.toUpperCase(),
    encodeURIComponent(endpoint),
    encodeURIComponent(parameterString)
  ].join('&');

  const signingKey = [
    encodeURIComponent(secrets.consumer),
    encodeURIComponent(secrets.token)
  ].join('&');

  return createHmac('sha1', signingKey).update(signatureBaseString).digest('base64');
}

module.exports = createSignature;