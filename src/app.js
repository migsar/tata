const { randomBytes } = require('crypto');

const Request = require('./request');
const createSignature = require('./utils');

class App {
  constructor({ credentials }) {
    this.credentials = credentials;
  }

  oauth(endpoint, { query }) {
    const timestamp = parseInt(Date.now() / 1000);
    const { consumer, token } = this.credentials;
    const oauthParams = {
      oauth_consumer_key: consumer.key,
      oauth_nonce: randomBytes(32).toString('hex'),
      oauth_signature_method: "HMAC-SHA1",
      oauth_timestamp: timestamp,
      oauth_token: token.key,
      oauth_version: "1.0"
    };
    const method = 'GET';// todo: add POST support
    const oauthData = {
      params: {
        ...oauthParams,
        ...query
      },
      secrets: {
        consumer: consumer.secret,
        token: token.secret
      }
    };

    const signedParams = {
      ...oauthParams,
      oauth_signature: createSignature(method, endpoint, oauthData)
    };
    
    const authStrings = Object.entries(signedParams)
      .sort((a, b) => a[0].localeCompare(b[0]))
      .map(entry =>
        `${encodeURIComponent(entry[0])}="${encodeURIComponent(entry[1])}"`
      );

    return `OAuth ${authStrings.join(', ')}`;
  }

  fetch(endpoint, options = {}) {
    const { query } = options;
    const authorization = this.oauth(endpoint, options);
    const requestOptions = {
      headers: {
        'Authorization': authorization
      }
    };

    const url = (query) ?
      `${endpoint}?${Object.entries(query).map(q => `${q[0]}=${q[1]}`).join('&')}` :
      endpoint;

    return new Request(url, requestOptions);
  }
}

module.exports = App; 