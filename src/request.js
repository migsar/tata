const https = require('https');
const JSONbig = require('json-bigint');

class Request {
  constructor(...params) {
    return this.createRequest(...params);
  }

  createRequest(url, options) {
    return new Promise((resolve, reject) => {
      https.get(url, options, (res) => {
        const { headers } = res;
        const contentType = headers['content-type'];
      
        if (!/^application\/json/.test(contentType)) {
          // Consume response data to free up memory
          res.resume();
          reject(new Error('Expected content-type is application/json.'));
        }

        res.setEncoding('utf8');
        let rawData = '';
        res.on('data', (chunk) => { rawData += chunk; });
        res.on('end', () => {
          try {
            const parsedData = JSONbig.parse(rawData);

            console.log(res.statusCode);
            if (res.statusCode === 200) {
              resolve(parsedData);
            } else {
              reject(parsedData);
            }
          } catch (e) {
            reject(e);
          }
        });
      });
    });
  }
}

module.exports = Request;