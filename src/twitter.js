const API = {
  FRIEND_IDS: 'https://api.twitter.com/1.1/friends/ids.json',
  USER_TIMELINE: 'https://api.twitter.com/1.1/statuses/user_timeline.json'
};

module.exports = {
  API
};