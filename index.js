require('dotenv').config();
const fs = require('fs');
const JSONbig = require('json-bigint');

const processed = require('./local/users.json');
const App = require('./src/app');
const { API } = require('./src/twitter');

const { CONSUMER_KEY, TOKEN_KEY, CONSUMER_SECRET, TOKEN_SECRET } = process.env;

// JSONbig is needed because ids are coded as numbers
const friends = JSONbig.parse(fs.readFileSync('./local/following.json', 'UTF-8'));
const ids = friends.ids.map(id => String(id));

const credentials = {
  consumer: {
    key: CONSUMER_KEY,
    secret: CONSUMER_SECRET
  },
  token: {
    key: TOKEN_KEY,
    secret: TOKEN_SECRET
  },
};

const app = new App({ credentials });

const processedIds = processed.map(entry => entry.id);
const unique = ids.filter(id => !processedIds.includes(id));
const toFetch = unique.slice(0, 600);
console.log(unique.length);

const result = [];
toFetch.forEach(id => {
  const query = {
    count: 1,
    user_id: id
  };

  app.fetch(API.USER_TIMELINE, { query })
  .then(data => {
    const { created_at, user } = data[0];

    const today = parseInt((new Date()).valueOf() / 1000);
    const lastPost = parseInt((new Date(data[0]['created_at'])).valueOf() / 1000);
    const year = 365 * 24 * 60 * 60;// A year in seconds

    // Inactive users are users that had not posted anything for 18 months
    const inactive = (today - lastPost) > (1.5 * year);

    const summary = {
      id: user.id_str,
      screenName: user.screen_name,
      age: user.created_at,
      url: user.url,
      location: user.location,
      description: user.description,
      followersCount: user.followers_count,
      friendsCount: user.friends_count,
      listedCount: user.listed_count,
      favouritesCount: user.favourites_count,
      statusesCount: user.statuses_count,
      lastPostAt: created_at,
      inactive
    };

    result.push(summary);
  })
  .catch(err => {
    console.log(`Error id: ${id}`);
    console.error(err);
  })
  .finally(() => {
    fs.writeFileSync('run.json', JSONbig.stringify(result));
  });
});
